# Setup

## .env

- `ALGOLIA_APP_ID`: Algolia App ID
- `ALGOLIA_API_KEY`: Algolia API key
- `API_ENV`: The target environment dev|test|prod
